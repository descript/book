default: install render serve
preview: render serve
preview-chrome-mac: render reload-chrome-mac serve

install:
	gem list | grep asciidoctor || gem install asciidoctor
	gem list | grep coderay || gem install coderay

render:
	rm -rf ./html && mkdir -p html
	asciidoctor -D html -a linkcss './src/**/*.adoc'

reload-chrome-mac:
	osascript -e "delay 0.75" -e 'tell application "Google Chrome" to reload active tab of window 1' &

serve:
	cd ./html && sleep 0.25 && ruby -run -e httpd . -p 9292

dev:
	nodemon -w ./src -w ./examples -e "adoc ds" --exec make preview

dev-mac:
	nodemon -w ./src -w ./examples -e "adoc ds" --exec make preview-chrome-mac

check_defined = \
	$(foreach 1,$1,$(__check_defined))
	__check_defined = \
		$(if $(value $1),, \
		$(error Missing required var: $1$(if $(value 2), ($(strip $2)))))

setup-server:
	@echo "Note: If this server is brand new you may need to SSH and update root password manually before running this command"
	@$(call check_defined, ip, Error: Please provide var ip=XYZ)
	@$(call check_defined, user, Error: Please provide var user=XYZ)
	@$(call check_defined, password, Error: Please provide var password=XYZ)
	@$(call check_defined, domain, Error: Please provide var domain=XYZ)
	@echo Connecting to ${ip} ...
	@ssh root@${ip} "useradd -m ${user}; echo '${user}:${password}' | chpasswd; echo 'Password updated for user ${user}!'"
	@ssh root@${ip} "apt-get update -yq; apt-get upgrade -yq; which nginx || apt-get install nginx"
	@scp ./misc/descript.book.nginx.conf root@${ip}:/etc/nginx/sites-available
	@ssh root@${ip} "cd /etc/nginx/sites-available; cat descript.book.nginx.conf | sed 's/@USER/${user}/g' | sed 's/@DOMAIN/${domain}/g' > tmp; cat tmp > descript.book.nginx.conf; rm -f tmp; ln descript.book.nginx.conf ../sites-enabled/; nginx -t; nginx -s reload && echo 'Your site should be successfully set up, and once deployed will be available at http://${domain}'"

deploy:
	@$(call check_defined, ip, Error: Please provide var ip=XYZ)
	@$(call check_defined, user, Error: Please provide var user=XYZ)
	@$(call check_defined, password, Error: Please provide var password=XYZ)

	apt-get update
	which expect || apt-get install -yq expect
	which rsync || apt-get install -yq rsync

	@echo 'spawn ssh ${user}@${ip} "echo \\"Connected to server\\""\nexpect "password:"\nexpect {\n"(yes/no)" { send "yes\\r";exp_continue }\n"password:"\n} send "${password}\\r"\nexpect "*\\r"\nexpect "\\r"' > ./misc/tmp.script
	@expect ./misc/tmp.script

	@echo 'spawn rsync -r ./html ${user}@${ip}:/home/${user}/descript.book.new\nexpect {\n"(yes/no)" { send "yes\\r";exp_continue }\n"password:"\n} send "${password}\\r"\nexpect "*\\r"\nexpect "\\r"' > ./misc/tmp.script
	@expect ./misc/tmp.script

	@echo 'spawn ssh ${user}@${ip} "cd /home/${user}; rm -rf descript.book && mv descript.book.new/html descript.book; rm -rf descript.book.new"\nexpect "password:"\nsend "${password}\\r"\nexpect "*\\r"\nexpect "\\r"' > ./misc/tmp.script
	@expect ./misc/tmp.script

	@rm ./misc/tmp.script
